## チュートリアルを終えたあとやったらいいこと

@tnir (Takuya Noguchi)
[Django Girls Tokyo #4](https://djangogirls.org/tokyo/) 2018-11-03

---

## Me

- [@tnir](https://github.com/tnir) / Tw: `@tn961ir` / FB: `takuya.noguchi.961`
- PyCon APAC 2013, JP 2014,2015,2016,2017,2018スポンサー担当
- Djangoユーザ (2006-)
- Djangoコントリビュータ (2018)
- gunicorn, pandasコントリビュータ (2016-)
- その他Go, Ruby on RailsアプリケーションOSS開発者

---

## Django Girls、終わってからやること

- CI/CD (自動化)
- Gitレポジトリ
- diversityイベントに参加する

---

## Today I learned:

```python

A = ['hello','world']
```

or

```python

A = ['hello', 'world']
```

---

## flake8

```python

A = ['hello','world']
```

↓↓↓↓↓↓↓↓↓↓

```python

A = ['hello', 'world']
```

---

## flake8

```bash
pip install -U flake8
```

```bash
flake8 settings.py
```

```
settings.py:1:13: E231 missing whitespace after ','
```

```
autopep8 -i settings.py
```
---

## CI/CD

- CircleCI https://circleci.com/

```.circleci/config.yml
run:
  - flake8 settings.py
```

---

## その先のGitレポジトリ

- `GitHub.com`
    - 公開レポジトリは無料（プライベートレポジトリは有料）
- `GitLab.com`
    - 公開レポジトリもプライベートレポジトリは無料
    - CI/CDも無料
    - ↑オススメ

---

## GitLab Issuesで管理

![](https://image.slidesharecdn.com/gitlab-issue-tracker-180410111749/95/gitlab-1-638.jpg?cb=1523360418)

- Qiita: https://qiita.com/jumpyoshim/items/f0ee99d770192c48fc7e
- スライド: https://www.slideshare.net/JumpeiYoshimura/gitlab-93438104

---

## GitLab Women

### GitLab Women (GitLab Meetup Tokyo #11)

![](https://connpass-tokyo.s3.amazonaws.com/thumbs/28/0e/280e086df3279ad2614b116b7f344453.png)

11/7(水) 秋葉原 https://gitlab-jp.connpass.com/event/105178/
